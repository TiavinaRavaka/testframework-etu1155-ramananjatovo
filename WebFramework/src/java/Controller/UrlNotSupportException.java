/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

/**
 *
 * @author Tiavina Ravaka
 */
public class UrlNotSupportException extends Exception{
    public UrlNotSupportException() {
    }
    /**
     * Constructs an instance of <code>UrlNotSupportException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public UrlNotSupportException(String msg) {
        super(msg);
    }
}
