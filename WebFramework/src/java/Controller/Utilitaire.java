/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import java.io.File;
import java.io.FilenameFilter;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Vector;
import java.net.URL;
/**
 *
 * @author Tiavina Ravaka
 */
public class Utilitaire {
    static String retriveUrlFromRawUrl(String rawUrl){
        return rawUrl.split("/")[rawUrl.split("/").length-1].split(".do")[0];
    }
    static HashMap<String,HashMap<String,String>> linkMapping(String[] listepkgs) throws PackageNotExist, Exception {
        HashMap<String,HashMap<String,String>> resultat = new HashMap<String,HashMap<String,String>>();
        for(String pkg : listepkgs){
            Vector<Class> listeClass;
            try {
                listeClass= getClass(pkg);
            } catch (PackageNotExist e) {
                continue;
            }
            for(Class cl : listeClass){
                Method[] listeMethod = cl.getDeclaredMethods();
                for(Method method : listeMethod ){
                    if(method.isAnnotationPresent(Url.class)){
                        HashMap<String,String> classMeth = new HashMap<>();
                        classMeth.put("class", cl.getName());
                        classMeth.put("method", method.getName());
                        resultat.put(method.getAnnotation(Url.class).name(),classMeth);
                        
                    }
                    
                }
            }
        }
        return resultat;
    }
    public static Vector<Class> getClass(String namepkg)throws PackageNotExist , Exception{
        Vector<Class> liste = new Vector<Class>(1,1);
        URL root= Thread.currentThread().getContextClassLoader().getResource(namepkg.replace(".", "/"));
        File fichier= null;
        
        try{
            fichier = new File(root.getFile());
        } catch (NullPointerException e) {
            throw new PackageNotExist();
        }
        FilenameFilter fltr =new FilenameFilter(){
            @Override
            public boolean accept(File dir, String name) {
                try {
                   return name.endsWith(".class"); 
                } catch (Exception e) {
                    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                
                }
        }
        };
        File[] files= fichier.listFiles(fltr);
        for (File file : files) {
            String className = file.getName().replaceAll(".class$", "");
            Class<?> cls = Class.forName(namepkg+ "." + className);
            boolean cond = !cls.equals(FrontServlet.class)|| !cls.equals(ViewModel.class);
            if(cond) {
                liste.add(cls);
            }
        }
        return liste;
    }

    public static void urlIsPresent(String url,HashMap<String, HashMap<String, String>> urlLink) throws UrlNotSupportException{
        
       if(!urlLink.containsKey(url)){
           throw new UrlNotSupportException("URL not supported");
           
       }
      
    }

    
    
}
