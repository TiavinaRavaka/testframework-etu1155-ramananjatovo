/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

/**
 *
 * @author Tiavina Ravaka
 */
public class PackageNotExist extends Exception{
   public PackageNotExist() {
    }

    /**
     * Constructs an instance of <code>PackageDoesNotExistException</code> with
     * the specified detail message.
     *
     * @param msg the detail message.
     */
  public PackageNotExist(String msg) {
        super(msg);
    }
}  

