/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import Controller.Url;
import Controller.ViewModel;

/**
 *
 * @author Tiavina Ravaka
 */
public class ModelTest {
    String nom;
    String prenom;
    String datenaiss;
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getDatenaiss() {
        return datenaiss;
    }

    public void setDatenaiss(String datenaiss) {
        this.datenaiss = datenaiss;
    }
    
     @Url(name="get")
    public ViewModel test1(){
        ViewModel result = new ViewModel();
        result.setUrlRedirect("list.jsp");
        result.getData().put("etu", "ETU1155");
        result.getData().put("nom", "Ramananjatovo");
        result.getData().put("prenom", "Ravaka Feno Tiavina");
        result.getData().put("text", "Vous avez reussit à trouver la page");
        return result;
    }
    
    @Url(name="testurl")
    public ViewModel test2(){
        ViewModel result = new ViewModel();
        result.setUrlRedirect("index.jsp");
        return result;
    }
    @Url(name="ajout")
    public ViewModel test3(){
       ViewModel result = new ViewModel();
        result.setUrlRedirect("retourdonne.jsp");
        result.getData().put("nom",nom);
        result.getData().put("prenom",prenom);
        result.getData().put("datenaiss",datenaiss);
        return result;
    }
}
